# DemoTypeDB

Pequeña demo de uso de la base de datos TypeDB para la presentación de la asignatura Web semántica y social.

## Comenzar
-  Descargar el proyecto. 
-  Arrancar el servidor de typeDB
-  Nos posicionamos en la carpeta del proyecto y ejecutamos:
    ***typedb console --script=reset_demo.tql***
-  Con esto reiniciaremos la base de datos y añadiremos unos ejemplos.

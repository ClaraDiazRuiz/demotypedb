define

## Business attributes

name sub attribute,
    value string;

email sub attribute,
    value string,
    regex "^[A-Za-z0-9+_.-]+@(.+)$";

telephone sub attribute,
    value string,
    regex "^34[6789]\d{8}$";

## offer attributes

validFrom sub attribute,
    value datetime;

validThrough sub attribute,
    value datetime;

price sub attribute,
    value double;

unitOfMeasurement sub attribute,
   value string,
   regex "kg|unit";

available sub attribute,
    value boolean;

leaf sub attribute,
    value boolean;

image sub attribute,
    value string,
    owns validFrom,
    owns validThrough;

description sub attribute,
    value string;

## Postal Address attributes

country sub attribute,
    value string,
    regex "^(EN|ES)$";
    
locality sub attribute,
    value string;

postalCode sub attribute,
    value string,
    regex "^[0-5]\d{4}$";

streetAddress sub attribute,
    value string;

# Attributes

contactPoint sub attribute,
    value string,
    abstract,
    owns email @key,
    owns telephone;

postalAddress sub contactPoint,
    owns country,
    owns locality,
    owns postalCode,
    owns streetAddress;

# Entities

businessEntity sub entity,
    owns name @key,
    owns description,
    plays offering:offers,
    plays hasLocation:isBusinessEntity;

product sub entity,
    owns name @key,
    owns leaf,
    plays offering:includes,
    plays is_a:class,
    plays is_a:subclass;

location sub entity,
    owns postalAddress,
    plays hasLocation:isPostalAddress;


# Relations

## Offering relations
offering sub relation,
    relates offers,
    relates includes,
    owns validFrom,
    owns validThrough,
    owns price,
    owns unitOfMeasurement,
    owns available,
    owns image,
    owns description;

is_a sub relation,
    relates class,
    relates subclass;

hasLocation sub relation,
    relates isBusinessEntity,
    relates isPostalAddress;

# Rules

rule isa_transitivity:
    when    {
        (class: $c1, subclass: $c2) isa is_a;
        (class: $c2, subclass: $c3) isa is_a;
    }
    then    {
        (class: $c1, subclass: $c3) isa is_a;
    };

rule isa_leaf:
    when    {
        $c1 isa product;
        not {(class: $c1, subclass: $c2) isa is_a;};
    }
    then    {
        $c1 has leaf true;
    };

rule offers_transitivity:
    when    {
        (offers: $be, includes: $c1) isa offering;
        (class: $c1, subclass: $c2) isa is_a;
    }
    then    {
        (offers: $be, includes: $c2) isa offering;
    };